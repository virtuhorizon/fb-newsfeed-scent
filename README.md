# README #

This is a python notebook script that uses facebook graph api 2.2 to get all the objects in your newsfeed and process the text and comments and then gives you some basic statistics about the sentiment of the content. 


*this is not a release, really just a test built in an hour while checking out the new graph api


* Some thoughts, what about what could be done

- Get all the text that you are shown, by friends or ads
- Crawl the links in the objects and rip the relevant text and process that too
- small database to avoid reprocessing and for aggregation over time ( sqlite is familliar for most people but NoSQL might be better in this case, since thats what facebook uses, easy migration, JSON to JSON..... good experiences with this: [CodernityDB NoSQL](http://labs.codernity.com/codernitydb/) )
- small neural net?

### How do I get set up? ###

What you'll need:

Facebook api authentication token with a bunch of permissions, check the Facebook developer app it'll generate one for you

python 2.7
Ipython notebook
Natural language processing library
textblob library
facebook-python-sdk

if you're thinking of getting into data analysis, I recommend installing everything with Anaconda, sets up a bunch of stuff nicely.

how?
once you have python + Ipython notebook installed, use get-pip.py to install the rest; whenever a missing dependency comes up just pop a console "pip install blahblah"
google it
O and make sure if you're on x64 Make sure that you install the x64 versions of ALL the modules, don't mix and match

### Contribution guidelines ###


Feel free to fork this, do something cooler!